Landing Page

We used the Google Drive for group communication so we store all documents on it, and everyone can check our documents with website links and Google account.
We put all links on the Gitlab as well, for convenience for you to use. As for the Gitlab, you need to have an ANU account and use it to login on to the Gitlab once, 
then I can add your account into our project group.

Link to project proposal:
https://drive.google.com/drive/folders/0B-aqayUXIJxyRlVzZmcyWmI2LUU

Link to project minutes:
https://drive.google.com/drive/folders/0B-aqayUXIJxyaTZaakE4eDV0RjA

Link to our Google Drive of the whole project documents:
https://drive.google.com/drive/folders/0B-aqayUXIJxyTV9NSGk3enVHWFU

And for easier to view our documentations, you can check the Landing Page of the Google Drive:
https://docs.google.com/document/d/1Z2ax9VJ4nUsgtnSYHi_D06w4GB-HnlQZneMzIGM9ysU/edit


Thanks for reading.
